FROM registry.gitlab.com/mefortunato/docker-images/rdkit:2020-09-4-py38-deb

COPY requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt && \
    rm /tmp/requirements.txt

COPY . /usr/local/mongochem

ENV PYTHONPATH "$PYTHONPATH:/usr/local/mongochem"