import time
from rdkit import Chem
from rdkit.Chem import AllChem
from tqdm import tqdm

from . import db
from . import utils


def time_fn(fn, *args, **kwargs):
    t0 = time.time()
    fn(*args, **kwargs)
    return time.time()-t0


def sample_molecules(n=1000, fields={'_id': True}):
    return db.MOLECULES.aggregate(
        [
            {'$sample': {'size': n}},
            {'$project': fields}
        ]
    )


def sample_one_molecule():
    return list(sample_molecules(1, fields={'smiles': True}))[0]['smiles']


def generate_fake_reactions(n=1000):
    random_mols_1 = [doc['_id'] for doc in sample_molecules(n)]
    random_mols_2 = [doc['_id'] for doc in sample_molecules(n)]
    for r1, r2 in tqdm(zip(random_mols_1, random_mols_2), total=n):
        db.REACTIONS.insert({
            '_id': utils.hash_smiles('.'.join([r1, r2])),
            'reactants': [r1, r2]
        })


def test_query(smiles=None, threshold=0.75):
    if not smiles:
        smiles = sample_one_molecule()
    qfp = utils.fingerprint_bits(smiles)
    similar = db.search_molecules(smiles, threshold)
    _ids = [mol['_id'] for mol in similar]
    return db.reactions_with_reactants_ids(_ids, qfp)

def test_similarity(smiles=None, threshold=0.75):
    if smiles is None:
        smiles = sample_one_molecule()
    results = db.search_molecules(smiles, threshold)
    for res in results:
        if utils.similarity(smiles, res['smiles']) - res['tanimoto'] >= 0.1:
            return {
                'smiles': smiles, 
                'results': results
            }
    