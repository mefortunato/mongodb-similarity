from typing import Optional, List

from fastapi import FastAPI, Query

from . import db
from . import utils

app = FastAPI()

@app.get("/", summary=" ")
def read_root():
    return {"Hello": "World"}

@app.get('/molecule/', summary="Get a molecule by _id")
def get_molecule(_id: Optional[str] = ''):
    """
    Get a molecule by `_id`
    - **_id**: molecule _id
    """
    return db.get_molecule(_id)

@app.get('/molecule/random/', summary="Get a random molecule")
def get_random_molecule():
    """
    Get a random molecule
    """
    return db.get_random_molecule()

@app.get('/molecules/', summary="Get multiple molecules by _ids")
def get_molecules(_ids: List[str] = Query(None)):
    """
    Get multiple moeclues by `_id`s
    - **_ids**: list of _ids
    """
    return db.get_molecules(_ids)

@app.get('/search/molecules/', summary="Search for molecules by tanimoto similarity")
def get_search_molecules(smiles: str, threshold: Optional[float] = 0.75):
    """
    Search for molecules by tanimoto similarity
    - **smiles**: smiles string for search query
    - **threshold**: minimum similarity threshold
    """
    return db.search_molecules(smiles, threshold=threshold)

@app.get('/search/reactions/', summary="Search for reactions by molecular similarity")
def get_search_reactions(smiles: str, threshold: Optional[float] = 0.75, species: Optional[str] = 'reactants'):
    """
    Search for reactions by reactant similarity
    - **smiles**: smiles string for search query
    - **threshold**: minimum similarity threshold
    - **species**: species to search (reactants, products)
    """
    return db.search_reactions(smiles, threshold=threshold, species=species)
