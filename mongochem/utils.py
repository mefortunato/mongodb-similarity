import json
import hashlib
from rdkit import Chem, DataStructs
from rdkit.Chem import AllChem
from typing import List, Union

def canonicalize_smiles(smiles: str) -> str:
    """
    Canonicalizes smiles string
    """
    mol = Chem.MolFromSmiles(smiles)
    [atom.SetAtomMapNum(0) for atom in mol.GetAtoms()]
    return Chem.MolToSmiles(mol)

def hash_smiles(smiles: str):
    """
    Hashes smiles string
    """
    return hashlib.sha256(smiles.encode()).hexdigest()

def hash_dictionary(dictionary: dict):
    """
    Hashes dictionary using serialized json representation
    """
    return hashlib.sha256(json.dumps(dictionary, sort_keys=True).encode()).hexdigest()

def fingerprint(molecule: Union[str, Chem.Mol]) -> DataStructs.ExplicitBitVect:
    """
    Generates Morgan fingerprint bit vector for smiles string or rdkit `Chem.Mol`
    """
    if isinstance(molecule, str):
        molecule = Chem.MolFromSmiles(molecule)
    fp = AllChem.GetMorganFingerprintAsBitVect(molecule, radius=2)
    return fp

def fingerprint_bits(molecule: Union[str, Chem.Mol]) -> List[int]:
    """
    Returns fingerprint bits for smiles string or rdkit `Chem.Mol`
    """
    fp = fingerprint(molecule)
    qfp = sorted(fp.GetOnBits())
    return qfp

def similarity(smi1: str, smi2: str):
    """
    Calculate tanimoto similarity between two smiles strings using Morgan fingerprint bit vectors
    """
    fp1 = fingerprint(smi1)
    fp2 = fingerprint(smi2)
    return DataStructs.TanimotoSimilarity(fp1, fp2)
