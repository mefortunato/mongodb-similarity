import os
from pymongo import MongoClient

user = os.environ.get('MONGODB_USERNAME', 'mongochem')
pw = os.environ.get('MONGODB_PASSWORD', 'mongochem')
host = os.environ.get('MONGO_HOST', 'mongodb')
port = os.environ.get('MONGO_PORT', 27017)
database = os.environ.get('MONGODB_DATABASE', 'mongochem')

URI = f'mongodb://{user}:{pw}@{host}:{port}'

client = MongoClient(URI)
db = client[database]
MOLECULES = db.molecules
REACTIONS = db.reactions
MOL_FP_COUNT = db.molecule_fp_count


def clean() -> None:
    """
    Utility function to clean database
    """
    MOLECULES.delete_many({})
    MOL_FP_COUNT.delete_many({})
    REACTIONS.delete_many({})
