from bson import Binary
from pymongo.results import UpdateResult
from rdkit import Chem
from rdkit.Chem import AllChem
from typing import List, Optional

from .db import MOLECULES, MOL_FP_COUNT
from ..utils import canonicalize_smiles, fingerprint_bits, hash_smiles, hash_dictionary


def delete_molecules(q: Optional[dict] = {}) -> None:
    """
    Utility function to delete molecules using query `q`. 
    Also decreases fp bit count for bits in each molecule.
    """
    molecules = MOLECULES.find(q)
    for mol in molecules:
        for bit in mol['bits']:
            MOL_FP_COUNT.update(q, {'$inc': {'count': -1}})
    MOLECULES.delete_many(q)


def add_smiles(smiles: str) -> UpdateResult:
    """
    Add smiles string to database
    """
    mol = Chem.MolFromSmiles(smiles)
    return add_molecule(mol)


def add_molecule(molecule: Chem.Mol) -> UpdateResult:
    """
    Add rdkit `Chem.Mol` to databse
    """
    bits = fingerprint_bits(molecule)
    count = len(bits)
    smiles = Chem.MolToSmiles(molecule)
    doc = {
        '_id': hash_smiles(smiles),
        'smiles': smiles,
        'rdmol': Binary(molecule.ToBinary()),
        'bits': bits,
        'count': count
    }
    result = MOLECULES.update_one(
        {'_id': doc['_id']},
        {'$set': doc},
        upsert=True
    )
    if result.upserted_id:
        existing_bits = [
            doc['_id']
            for doc in MOL_FP_COUNT.find(
                {'_id': {'$in': bits}},
                {'_id': True}
            )
        ]
        MOL_FP_COUNT.update_many(
            {'_id': {'$in': existing_bits}},
            {'$inc': {'count': 1}}
        )

        bits_to_create = list(set(bits) - set(existing_bits))
        if bits_to_create:
            MOL_FP_COUNT.insert_many(
                [
                    {'_id': _id, 'count': 1}
                    for _id in bits_to_create
                ]
            )
    return result


def get_molecule(_id: Optional[str] = '') -> dict:
    """
    Get molecule from atabse by `_id`
    """
    q = {'_id': _id} if _id else {}
    return MOLECULES.find_one(q, {'smiles': True})


def get_random_molecule(fields: Optional[dict] = {'smiles': True}) -> dict:
    """
    Get a ranom molecule from the database returning fields in `fields`
    """
    mol = list(MOLECULES.aggregate(
        [
            {'$sample': {'size': 1}},
            {'$project': fields}
        ]
    ))
    if len(mol) == 1:
        return mol[0]
    else:
        return {}



def get_molecules(_id_list: List[str]) -> List[dict]:
    """
    Get molecules by `_id`s in `_id_list`
    """
    return list(MOLECULES.find({'_id': {'$in': _id_list}}, {'smiles': True}))


def search_molecules(smiles: str, threshold: Optional[float] = 0.75) -> List[dict]:
    """
    Search for molecules in database with similarity to `smiles` greater than `threshold`. Calculates tanimoto similarity and returns similarities with result
    """
    qfp = fingerprint_bits(smiles)
    # Number of bits in query fingerprint
    qn = len(qfp)
    # Minimum number of bits in results fingerprints
    qmin = int(qn * threshold)
    # Maximum number of bits in results fingerprints
    qmax = int(qn / threshold)
    ncommon = qn - qmin + 1
    # reqbits = qfp[:ncommon]
    reqbits = [count['_id'] for count in MOL_FP_COUNT.find(
        {'_id': {'$in': qfp}}).sort('count', 1).limit(ncommon)]
    aggregate = [
        {'$match': {'count': {'$gte': qmin, '$lte': qmax},
                    'bits': {'$in': reqbits}}},
        {'$project': {
            'tanimoto': {'$divide': [
                {'$size': {'$setIntersection': ['$bits', qfp]}},
                {'$size': {'$setUnion': ['$bits', qfp]}}
            ]},
            'smiles': 1,
            '_id': 1,
        }},
        {'$match':  {'tanimoto': {'$gte': threshold}}}
    ]
    result = list(MOLECULES.aggregate(aggregate))

    return result