from pymongo.results import UpdateResult
from typing import List, Optional

from .db import REACTIONS
from .molecules import add_smiles, search_molecules
from ..utils import canonicalize_smiles, fingerprint_bits, hash_dictionary, hash_smiles


def delete_reactions(q: Optional[dict] = {}) -> None:
    """
    Utility function to delete reactions from database
    """
    REACTIONS.delete_many(q)


def add_reaction(
    reaction_smiles: str, 
    source: Optional[str] = None, 
    namerxn: Optional[str] = None, 
    metadata: Optional[dict] = {}
) -> UpdateResult:
    """
    Adds a reaction to the database. Metadata is optional dictionary of extra properties.
    """
    reactants, spectators, products = list(map(
        lambda x: [canonicalize_smiles(smi) for smi in x.split('.')], 
        reaction_smiles.split('>')
    ))

    canon_rsmi = f"{'.'.join(sorted(reactants))}>{'.'.join(sorted(spectators))}>{'.'.join(sorted(products))}"
    hashed_rsmi = hash_smiles(canon_rsmi)

    reactants_ids = []
    spectators_ids = []
    products_ids = []

    for smiles in reactants:
        reactants_ids.append(hash_smiles(smiles))
        add_smiles(smiles)

    
    for smiles in spectators:
        spectators_ids.append(hash_smiles(smiles))
        add_smiles(smiles)

    
    for smiles in products:
        products_ids.append(hash_smiles(smiles))
        add_smiles(smiles)

    doc = {
        'reaction_smiles': reaction_smiles,
        'canonical_reaction_smiles': canon_rsmi,
        'reactants': reactants_ids,
        'spectators': spectators_ids,
        'products': products_ids,
        'source': source,
        'namerxn': namerxn,
        'metadata': metadata
    }
    doc['_id'] = hash_dictionary(doc)

    result = REACTIONS.update_one(
        {'_id': doc['_id']},
        {'$set': doc},
        upsert=True
    )
    return result

def search_reactions(
    smiles: str, 
    threshold: Optional[float] = 0.75, 
    species: Optional[str] = 'reactants'
) -> List[dict]:
    """
    Search for reactions with component in `species` by similarity to `smiles` greater than `threshold`
    """
    qfp = fingerprint_bits(smiles)
    similar = search_molecules(smiles, threshold)
    _ids = [mol['_id'] for mol in similar]
    return reactions_with_species_ids(_ids, qfp, species=species)


def reactions_with_species_ids(
    _ids: List[str], 
    qfp: List[int], 
    species: Optional[str] = 'reactants'
) -> List[dict]:
    """
    Get reactions from database with component in `species` with an _id in `_ids`. Calculates tanimoto similarity using queary fingerprint `qfp`
    """
    aggregate = [
        {'$match': {species: {'$in': _ids}}},
        {
            '$lookup': 
                {
                    'from': 'molecules', 
                    'localField': species, 
                    'foreignField': '_id', 
                    'as': species
                }
        },
        {
            '$unwind': f'${species}'
        },
        {
            '$project': {
                f'{species}.tanimoto': {
                    '$divide': [
                        {'$size': {'$setIntersection': [f'${species}.bits', qfp]}},
                        {'$size': {'$setUnion': [f'${species}.bits', qfp]}}
                    ]
                },
                species: {'_id': True, 'smiles': True},
                'metadata': True,
                'reaction_smiles': True,
                'namerxn': True,
                'source': True
            }
        },
        {
            '$group': {
                '_id': '$_id',
                species: {'$push': f'${species}'},
                'metadata': {'$first': '$metadata'},
                'reaction_smiles': {'$first': '$reaction_smiles'},
                'namerxn': {'$first': '$namerxn'},
                'source': {'$first': '$source'},
            }
        }
    ]
    return list(REACTIONS.aggregate(aggregate))