import gzip
import pickle
import shutil
import urllib.request as request
from contextlib import closing
from pathlib import Path
from rdkit import Chem
from tqdm import tqdm
from typing import Optional
from zipfile import ZipFile

from .db import db, add_smiles, add_molecule, add_reaction


def download_file(url: str, path: str) -> None:
    """
    Utility function to download a file from `url` and save it to `path`
    """
    p = Path(path)
    p.parent.mkdir(parents=True, exist_ok=True)
    with closing(request.urlopen(url)) as r:
        with open(path, 'wb') as f:
            shutil.copyfileobj(r, f)


def download_uspto_50k(
    url: Optional[str] = 'https://ndownloader.figstatic.com/collections/2282464/versions/1', 
    path: Optional[str] ='data/uspto_50k.zip'
) -> None:
    """
    Utility function for downloading the uspto_50k dataset
    """
    download_file(url, path)


def download_chembl(
    url: Optional[str] = 'ftp://ftp.ebi.ac.uk//pub/databases/chembl/ChEMBLdb/releases/chembl_19/chembl_19.sdf.gz',
    path: Optional[str] = 'data/chembl_19.sdf.gz'
) -> None:
    """
    Utility function for downloading chembl
    """
    download_file(url, path)


def load_sdf(sdf: file, limit: Optional[int] = 10000) -> None:
    """
    Utility function to load sdf `file` object into database
    """
    n = 0 
    for mol in tqdm(Chem.ForwardSDMolSupplier(sdf), total=limit):
        if not mol:
            continue
        add_molecule(mol)
        n += 1
        if n >= limit:
          break


def load_chembl(path: Optional[str] = 'data/chembl_19.sdf.gz', limit: Optional[int] = 100000) -> None:
    """
    Utility function to read gzipped chembl sdf file into database 
    """
    with gzip.open(path) as sdf:
        load_sdf(sdf, limit)


def load_uspto_50k(path: Optional[str] = 'data/uspto_50k.zip') -> None:
    """
    Utility function to load zipped uspto_50k dataset into database
    """
    reactions = []
    with ZipFile(path) as zip:
        with ZipFile(zip.open('ci5006614_si_002.zip')) as data:
            with gzip.open(data.open('ChemReactionClassification/data/training_test_set_patent_data.pkl.gz')) as f:
                while True:
                    try:
                        reactions.append(pickle.load(f))
                    except EOFError:
                        break
    for reaction in tqdm(reactions):
        reaction_smiles, source, namerxn = reaction
        add_reaction(
            reaction_smiles=reaction_smiles, 
            source=source, 
            namerxn=namerxn
        )
